$( document ).ready(function() {
//exporte les données sélectionnées
    var $table = $('#table');
    var passager = '';
    $(function () {
        $('#toolbar').find('select').change(function () {
            $table.bootstrapTable('refreshOptions', {
                exportDataType: $(this).val()
            });
        });
    })

	var trBoldBlue = $("table");

	$(trBoldBlue).on("click", "tr", function (){
			$(this).toggleClass("bold-blue");
	});

    $(".side-menu a").click(function() {
        $(this).tab("show")
    });

    $('.col-passenger').on('click', function() {
        $('.col-passenger.active').removeClass('active');
        $(this).addClass('active');
    });
    $('.btn-class').on('click',function(e){
        e.preventDefault()
        $('.view-filter').hide();
        $('.sort-class').show();
        
    });
    $('.btn-stasiun').on('click',function(){
        $('.view-filter').hide();
        $('.sort-stasiun').show();
    });
    $('.btn-kereta').on('click',function(){
        $('.view-filter').hide();
        $('.sort-kereta').show();
    }); 
    $('.btn-waktu').on('click',function(){
        $('.view-filter').hide();
        $('.sort-time').show();
    });
    $(document).on('change','.checkbox',function(){
        $('.list-kereta').hide();
        var lenghtCheckbox= $('.checkbox:checked').length;
        if(lenghtCheckbox > 0){
            $('.checkbox').each(function(){
                if(this.checked==true){
                    var value=$(this).val();
                    $('.list-kereta').each(function(){
                        if($(this).attr('data-class')==value)
                        {
                            $(this).show();
                        }
                        if($(this).attr('data-stasiun')==value)
                        {
                            $(this).show();
                        }
                        if($(this).attr('data-kereta')==value)
                        {
                            $(this).show();
                        }
                        if($(this).attr('data-time')==value)
                        {
                            $(this).show();
                        }
                    });
                }
            })
        }else{
            $('.list-kereta').show();
        }
        
    });
    $(document).on('change','.radio-type-kereta',function(){
        var typeKereta = $(this).val();
        var slickGoTo  = $(this).attr('data-row');
        passager = $(this).attr('data-passenger');
        $('.col-passenger.active').find('.type-kereta').html(typeKereta);
        $('.content-column-sheet').slick('slickGoTo', slickGoTo);
      
    });
    $(document).on('click','.seat',function(){
        if(passager!=''){
            var dataActive= $(this).attr('data-active');
            var dataSeat = $(this).attr('data-row');
            var datagerbong =$(this).attr('data-row')+''+ $(this).attr('data-gerbang');
            $('.seat').each(function(){
                if($(this).attr('data-passenger')==passager){
                    $(this).removeClass('active');
                    $(this).html(' ');
                    $(this).attr('data-active','0');
                    $(this).attr('data-passenger','0');
                }
            });
            if(dataActive==1){
                $(this).removeClass('active');
                $(this).html(' ');
                $(this).attr('data-active','0');
                $(this).attr('data-passenger','0');
            }else{
                $(this).addClass('active');
                $(this).html('P'+passager);
                $(this).attr('data-active','1');
                $(this).attr('data-passenger',passager);
                $('.col-passenger.active').find('.seat-gerbong').html(datagerbong);
            }
        }
        
    });
    $('.content-column-sheet').slick({
        dots: false,
        prevArrow: '<div class="prev"><i class="fas fa-chevron-left"></i></div>',
        nextArrow: '<div class="next"><i class="fas fa-chevron-right"></i></div>'
    });

   
});    